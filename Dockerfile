FROM node:11 as build
WORKDIR /usr/src/app
COPY package.json .
COPY package-lock.json .
RUN npm ci
COPY ./public ./public
COPY ./src ./src
RUN npm run build

FROM nginx:mainline-alpine
COPY --from=build /usr/src/app/build /var/www
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80